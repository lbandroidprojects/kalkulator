package com.example.lukaszb.kalkulator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Button bPlus, bMinus;
    private EditText et1, et2;
    private TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bPlus = findViewById(R.id.bPlus);
        bPlus.setOnClickListener(this);
        bMinus = findViewById(R.id.bMinus);
        bMinus.setOnClickListener(this);
        et1 = findViewById(R.id.et1);
        et2 = findViewById(R.id.et2);
        tv = findViewById(R.id.tv);
    }

    @Override
    public void onClick(View view) {
        String text1 = et1.getText().toString();
        String text2 = et2.getText().toString();
        int result;
        if ( text1.length()==0 || text2.length()==0){
            return;
        }

        switch (view.getId()){
            case R.id.bPlus:
                result = Integer.parseInt(text1) + Integer.parseInt(text2);
                tv.setText(Integer.toString(result));
                break;
            case R.id.bMinus:
                result = Integer.parseInt(text1) - Integer.parseInt(text2);
                tv.setText(Integer.toString(result));
                break;
        }

    }
}
